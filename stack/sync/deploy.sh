#!/usr/bin/env bash

set -eux -o pipefail

CLUSTER_NAME="" 

context=$(kubectl config current-context)

if [[ $context =~ "eks" ]]; then
  export CLUSTER_NAME=$(kubectl config current-context |  sed 's:.*/::')
  echo "Connected to EKS cluster ${CLUSTER_NAME}."
else
  echo "Check that you are connected to an EKS cluster to proceed."
  exit 5
fi

[ ! -z "$CLUSTER_NAME" ] || (echo "This variable needs to be populated" && exit 5)

kubectl get ns foundation-install 2>/dev/null || kubectl create ns foundation-install

kubectl get ns common-install 2>/dev/null || kubectl create ns common-install

kubectl get crd apps.kappctrl.k14s.io 2>/dev/null || (curl -SsL https://github.com/carvel-dev/kapp-controller/releases/download/v0.51.0/release.yml -o- | sed 's/kapp-controller-packaging-global/tanzu-package-repo-global/' > /tmp/kapp-controller.yaml && kapp deploy -a kapp-controller.app -n common-install -f <(ytt -f /tmp/kapp-controller.yaml) -y && rm -rf /tmp/kapp-controller.yaml)

kapp deploy -c -a foundation-install.app -n foundation-install -f <(sops exec-file --no-fifo values/secrets.ghost.sops.yaml "ytt -f manifests --data-values-file {} -v cluster.name=${CLUSTER_NAME}")