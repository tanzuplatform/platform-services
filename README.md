# Common Stack

The common stack is deployed onto every cluster.  This is still in an app form due to the nature of the SOPS encrypted files.

## Release Process

This repository is rolled forward based on tagging.  To create a new release, push all your changes up to here and cut a new tag that is semver further along than the current one.

## Secrets

Secrets are encrypted using [AGE](https://age-encryption.org/), if you need access to the secret key you can either contact Robert Van Voorhees, or provide a new public key so that everything can be reencrypted with that one as well.
## I want this

Do you also want a Common Stack, but you don't want your clusters managed for you?

1. Look at `direct.yaml`.  If you apply this into your cluster you will be reconciling from this repository the latest tags.  **Note: since you are applying the secrets directly instead of from this git repository if the format changes things will break.**
2. Recommend instead forking this repository and changing the `refSelection` to `ref` corresponding to the branch name.

Alternatively, it's all just Carvel, so go ahead and deploy this directly:

```
kapp deploy -a rv2-phantom-common-apps-ctrl -n kapp-controller -f <(ytt -f stack/common/manifests --data-values-file stack/common/values/ghost)
```

Change the reference to stack/common/values/ghost to your own location, it can override anything in `stack/common/manifests/values.yaml` or you can change things in there as well.